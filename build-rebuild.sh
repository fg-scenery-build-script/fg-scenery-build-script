#! /bin/bash 

# this gets the config. Actually runs the config script
source ./config.sh

rm -r "$PWD/work/*"
rm -r "$PWD/output/Terrain"

init_time=$SECONDS
for i in ./data/SRTM-3/*.hgt; do hgtchop 3 "$i" "$PWD/work/SRTM-3"; done
hgtchop_time=$SECONDS
terrafit -j $threads -e 5 -x 20000 "$PWD/work/SRTM-3"
terrafit_time=$SECONDS

genapts850 --input="$PWD/data/airports/apt.dat" \
--work="$PWD/work" --threads=$threads \
--min-lon="$(bc <<< $(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $min_lon))" \
--max-lon="$(bc <<< $(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $max_lon))" \
--min-lat="$(bc <<< $(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $min_lat))" \
--max-lat="$(bc <<< $(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $max_lat))" \
--dem-path=SRTM-3/

for ICAO in ${!airports[*]}
do
    if [ -f "$PWD/data/airports/${airports[$ICAO]}.apt.dat" ]
    then
        genapts850 --input="$PWD/data/airports/${airports[$ICAO]}.apt.dat" \
        --work="$PWD/work" --threads=$threads --dem-path=SRTM-3
    elif [ -f "$PWD/data/airports/${airports[$ICAO]}.dat" ]
    then
        genapts850 --input="$PWD/data/airports/${airports[$ICAO]}.dat" \
        --work="$PWD/work" --threads=$threads --dem-path=SRTM-3
    else
        echo "$PWD/work/airports/${airports[$ICAO]}apt.dat or $PWD/data/airports/${airports[$ICAO]}.dat was not found so not building $ICAO airport. \
        be sure you place the airport(s) that you've specified in the config.sh into the $PWD/data/airports/ directory as ICAO.apt.dat"
    fi
done
genapts850_time=$SECONDS

for i in "${!areatypes[@]}"
    do
        if [ -d "$PWD/data/shapefiles/$i" ]
            then
                if [[ ${line_widths[$i]} ]]
                    then
                    ogr-decode --max-segment 500 --area-type ${areatypes[$i]} --line-width ${line_widths[$i]} "$PWD/work/${areatypes[$i]}" "$PWD/data/shapefiles/$i"
                else
                    ogr-decode --threads $threads --max-segment 500 --area-type ${areatypes[$i]} "$PWD/work/${areatypes[$i]}" "$PWD/data/shapefiles/$i"
                fi
        fi
    done
ogrdecode_time=$SECONDS

tg-construct --threads=$threads --work-dir=./work \
--output-dir=./output/Terrain \
--min-lon="$(bc <<< $(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $( [[ ${min_lon:0:1} = "W" ]] && echo $max_lon || echo $min_lon )))" \
--max-lon="$(bc <<< $(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $( [[ ${max_lon:0:1} = "W" ]] && echo $min_lon || echo $max_lon )))" \
--min-lat="$(bc <<< $(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $( [[ ${min_lat:0:1} = "S" ]] && echo $max_lat || echo $min_lat )))" \
--max-lat="$(bc <<< $(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $( [[ ${max_lat:0:1} = "S" ]] && echo $min_lat || echo $max_lat )))" \
`ls "$PWD/work/"`
tgconstruct_time=$SECONDS

convertsecs() {
 ((h=${1}/3600))
 ((m=(${1}%3600)/60))
 ((s=${1}%60))
 printf "%02d:%02d:%02d\n" $h $m $s
}

echo "hgtchop completed in      : $(convertsecs $(( $hgtchop_time - $init_time )))
terrafit completed in     : $(convertsecs $(( $terrafit_time - $hgtchop_time )))
genapts850 completed in   : $(convertsecs $(( $genapts850_time - $terrafit_time )))
ogr-decode completed in   : $(convertsecs $(( $ogrdecode_time - $genapts850_time )))
tg-construct completed in : $(convertsecs $(( $tgconstruct_time - $ogrdecode_time )))
entire script completed in: $(convertsecs $SECONDS)"
