Scenery build scripts for flighgear scenery
======

#### dependencies
sed, wget, bc, terragear, unzip, and gunzip.

Issues / Todo / Status
------
**Issues**
* None that I know of

**Todo**
* Add option to use more landclass types and to be able to specify layers to download
* Test the script with other versions of terragear than the latest from git master branch