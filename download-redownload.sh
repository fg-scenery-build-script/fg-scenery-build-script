#! /bin/bash

# this gets the config. Actually runs the config script
source ./config.sh

# creating the required directories
mkdir -p data work output
mkdir -p data/shapefiles
mkdir -p data/airports

# I download some extra evelation data to be sure I cover the area. -c option is to resume partial downloads.
for x in $(eval echo {$(expr ${min_lon:1:3} - 1)..$(expr ${max_lon:1:3} + 1)})
do for y in $(eval echo {$(expr ${min_lat:1:2} - 1)..$(expr ${max_lat:1:2} + 1)})
do wget -c "http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/${continent}/\
${min_lat:0:1}`printf "%0*d" 2 $y`\
${min_lon:0:1}`printf "%0*d" 3 $x`.hgt.zip" -P "$PWD/data/SRTM-3/"
done
done

unzip "$PWD/data/SRTM-3/*.zip" -d "$PWD/data/SRTM-3/"

# Get latest apt.dat
wget -O "$PWD/data/airports/apt.dat.gz" "http://sourceforge.net/p/flightgear/fgdata/ci/next/tree/Airports/apt.dat.gz?format=raw"
gunzip "$PWD/data/airports/apt.dat.gz"

# get shapefiles I subtract 1 from min lat & lon and add 1 to max lat & lon to get extra
wget -O "$PWD/data/shapefiles/cs-\
${min_lat:0:1}`printf "%0*d" 2 $(expr ${min_lat:1:2} - 1)`\
${min_lon:0:1}`printf "%0*d" 3 $(expr ${min_lon:1:3} - 1)`-\
${max_lat:0:1}`printf "%0*d" 2 $(expr ${max_lat:1:2} + 1)`\
${max_lon:0:1}`printf "%0*d" 3 $(expr ${max_lon:1:3} + 1)`.zip"\
 "http://mapserver.flightgear.org/dlshp?layer=cs\
&ymin=$(bc <<< "$(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $min_lat) - 1")\
&ymax=$(bc <<< "$(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $max_lat) + 1")\
&xmin=$(bc <<< "$(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $min_lon) - 1")\
&xmax=$(bc <<< "$(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $max_lon) + 1")"

wget -O "$PWD/data/shapefiles/v0_landmass-\
${min_lat:0:1}`printf "%0*d" 2 $(expr ${min_lat:1:2} - 1)`\
${min_lon:0:1}`printf "%0*d" 3 $(expr ${min_lon:1:3} - 1)`-\
${max_lat:0:1}`printf "%0*d" 2 $(expr ${max_lat:1:2} + 1)`\
${max_lon:0:1}`printf "%0*d" 3 $(expr ${max_lon:1:3} + 1)`.zip"\
 "http://mapserver.flightgear.org/dlsingle?layer=v0_landmass\
&ymin=$(bc <<< "$(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $min_lat) - 1")\
&ymax=$(bc <<< "$(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $max_lat) + 1")\
&xmin=$(bc <<< "$(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $min_lon) - 1")\
&xmax=$(bc <<< "$(sed -e 's/[WS]/\-/g' -e 's/[EN]//g' <<< $max_lon) + 1")"

# the following loops through the list of shp files in the zip
# file and for each creates a directory with same name (.shp excluded)
# and unzips all files into the correct directories
for i in $(unzip -l "$PWD/data/shapefiles/cs-\
${min_lat:0:1}`printf "%0*d" 2 $(expr ${min_lat:1:2} - 1)`\
${min_lon:0:1}`printf "%0*d" 3 $(expr ${min_lon:1:3} - 1)`-\
${max_lat:0:1}`printf "%0*d" 2 $(expr ${max_lat:1:2} + 1)`\
${max_lon:0:1}`printf "%0*d" 3 $(expr ${max_lon:1:3} + 1)`.zip" | sed -n 's/.*\(cs.*shp\)/\1/p')
do mkdir -p "$PWD/data/shapefiles/${i//\.shp/}"
unzip "$PWD/data/shapefiles/cs-\
${min_lat:0:1}`printf "%0*d" 2 $(expr ${min_lat:1:2} - 1)`\
${min_lon:0:1}`printf "%0*d" 3 $(expr ${min_lon:1:3} - 1)`-\
${max_lat:0:1}`printf "%0*d" 2 $(expr ${max_lat:1:2} + 1)`\
${max_lon:0:1}`printf "%0*d" 3 $(expr ${max_lon:1:3} + 1)`.zip" "${i//\.shp}*" -d "$PWD/data/shapefiles/${i//\.shp/}"
done

for i in $(unzip -l "$PWD/data/shapefiles/v0_landmass-\
${min_lat:0:1}`printf "%0*d" 2 $(expr ${min_lat:1:2} - 1)`\
${min_lon:0:1}`printf "%0*d" 3 $(expr ${min_lon:1:3} - 1)`-\
${max_lat:0:1}`printf "%0*d" 2 $(expr ${max_lat:1:2} + 1)`\
${max_lon:0:1}`printf "%0*d" 3 $(expr ${max_lon:1:3} + 1)`.zip" | sed -n 's/.*\(v0.*shp\)/\1/p')
do mkdir -p "$PWD/data/shapefiles/${i//\.shp/}"
unzip "$PWD/data/shapefiles/v0_landmass-\
${min_lat:0:1}`printf "%0*d" 2 $(expr ${min_lat:1:2} - 1)`\
${min_lon:0:1}`printf "%0*d" 3 $(expr ${min_lon:1:3} - 1)`-\
${max_lat:0:1}`printf "%0*d" 2 $(expr ${max_lat:1:2} + 1)`\
${max_lon:0:1}`printf "%0*d" 3 $(expr ${max_lon:1:3} + 1)`.zip" "${i//\.shp}*" -d "$PWD/data/shapefiles/${i//\.shp/}"
done

convertsecs() {
 ((h=${1}/3600))
 ((m=(${1}%3600)/60))
 ((s=${1}%60))
 printf "%02d:%02d:%02d\n" $h $m $s
}

echo "$0 completed in: $(convertsecs $SECONDS)"
